package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhHard02.swf",symbol="WWSound_OhHard02")]
	
	public dynamic class WWSound_OhHard02 extends Sound {
		
		public function WWSound_OhHard02() {
			super();
		}
	}

}
