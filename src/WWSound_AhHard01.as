package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_AhHard01.swf",symbol="WWSound_AhHard01")]
	
	public dynamic class WWSound_AhHard01 extends Sound {
		
		public function WWSound_AhHard01() {
			super();
		}
	}

}
