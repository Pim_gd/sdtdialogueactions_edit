package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class BounceTriggers extends CustomTriggerHolder
	{
		private const defaultBouncePower:Number = 0.15;
		public function BounceTriggers(G:Object,M:Main) 
		{
			super(G, M);
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("BOUNCE_TITS", 0, new FunctionObject(bounceTitsZeroArgs, this, []));
			t.registerTrigger("BOUNCE_TITS", 1, new FunctionObject(bounceTits, this, []));
		}
		
		public function bounceTitsZeroArgs(...args):void {
			bounceTits(defaultBouncePower);
		}
		
		public function bounceTits(power:Number):void {
			g.her.leftBreastController.accelerate(power);
			g.her.rightBreastController.accelerate(power);
		}
		
	}

}