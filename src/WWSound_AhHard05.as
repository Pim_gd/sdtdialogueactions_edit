package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_AhHard05.swf",symbol="WWSound_AhHard05")]
	
	public dynamic class WWSound_AhHard05 extends Sound {
		
		public function WWSound_AhHard05() {
			super();
		}
	}

}
