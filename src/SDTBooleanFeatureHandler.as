package 
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class SDTBooleanFeatureHandler 
	{
		private var m:Main;
		private var daVariableName:String;
		private var objectTargeted:Object;
		private var keyTargeted:String;
		private var clickFunction:FunctionObject;
		
		public function SDTBooleanFeatureHandler(m:Main, variableName:String, target:Object, key:String, clickFunc:FunctionObject) 
		{
			this.m = m;
			daVariableName = variableName;
			objectTargeted = target;
			keyTargeted = key;
			clickFunction = clickFunc;
		}
		
		public function getVariableName():String {
			return daVariableName;
		}
		
		/**
		 * Checks input for 1 or 0, returns true/false if OK or null if error.
		 * @param	value Value to parse
		 * @return true or false if the parsing went okay, null if error.
		 */
		public function handleBooleanVariableWrite(value:*):Object {
			if (value is Number) {
				if (value == 1) {
					return true;
				} else if (value == 0) {
					return false;
				} else {
					m.displayMessageRed("Unrecognized value (" + value + ") for "+daVariableName);
				}
			} else {
				m.displayMessageRed("Unrecognized value (" + value + ") for "+daVariableName);
			}
			return null;
		}
		
		public function isEnabledAsUint(...args):uint {
			return isEnabled(args) ? 1 : 0;
		}
		
		public function isEnabled(...args):Boolean {
			return objectTargeted[keyTargeted];
		}
		
		public function setEnabledness(...args):void {
			var value:Object = handleBooleanVariableWrite(args[0]);
			if (value != null && value != isEnabled()) {
				clickFunction.call();
			}
		}
	}

}