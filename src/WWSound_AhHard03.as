package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_AhHard03.swf",symbol="WWSound_AhHard03")]
	
	public dynamic class WWSound_AhHard03 extends Sound {
		
		public function WWSound_AhHard03() {
			super();
		}
	}

}
