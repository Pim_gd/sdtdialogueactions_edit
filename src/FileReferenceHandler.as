package 
{
	/**
	 * Handles all the crap regarding file references, like targetting different character folders
	 * Mainly intended to put all the file path related stuff in one place
	 * @author Pimgd
	 */
	public class FileReferenceHandler 
	{
		private var m:Main;
		public function FileReferenceHandler(main:Main) 
		{
			m = main;
		}
		
		public function convertFilePath(original:String):String {
			if (original == "" || original == null) {
				return "";
			}
			
			if (StringFunctions.stringStartsWith(original, "$")) {
				return "Mods/"+original.substr(1);
			} else {
				return "Mods/" + m.lastLoadedCData + "/" + original;
			}
		}
	}

}