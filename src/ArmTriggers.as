package  
{
	import flash.events.Event;
	import flash.geom.Point;
	/**
	 * ...
	 * @author Pimgd
	 */
	public class ArmTriggers extends CustomTriggerHolder
	{
		private var rubbing:Boolean = false;
		private var delta:Number = 0;
		public function ArmTriggers(G,M) 
		{
			super(G, M);
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("ARMS_HIS_CROTCH", 0, new FunctionObject(armsHisCrotch, this, []));
			t.registerTrigger("ARMS_CROTCH", 0, new FunctionObject(armsCrotch, this, []));
			t.registerTrigger("RIGHT_ARM_BREAST", 0, new FunctionObject(rightArmBreast, this, []));
			t.registerTrigger("LEFT_ARM_BREAST", 0, new FunctionObject(leftArmBreast, this, []));
			t.registerTrigger("ARMS_BREAST", 0, new FunctionObject(armsBreast, this, []));
			t.registerTrigger("RUB_HIS_CROTCH_ON", 0, new FunctionObject(rubOn, this, []));
			t.registerTrigger("RUB_HIS_CROTCH_OFF", 0, new FunctionObject(rubOff, this, []));
			t.registerTrigger("FIX_ARMS", 0, new FunctionObject(fixArms, this, []));
			t.registerTrigger("FIX_LEFT_ARM", 0, new FunctionObject(fixLeftArm, this, []));
			t.registerTrigger("FIX_RIGHT_ARM", 0, new FunctionObject(fixRightArm, this, []));
		}
		
		public function fixArms(...args):void {
			fixLeftArm();
			fixRightArm();
		}
		
		public function fixLeftArm(...args):void {
			g.her.leftArmIK.reversedEnd = false;
		}
		
		public function fixRightArm(...args):void {
			g.her.rightArmIK.reversedEnd = false;
			rubOff();
		}
		
		public function rightArmBreast(...args):void {
			if(m.main.animtools_comm && m.main.animtools_comm.versionnum > 31)
			{
				g.her.setRightArmPosition(5, true);   //5 is cupping
			}
			else
			{
			g.her.setRightArmPosition(1, true);	
			g.her.rightArmIK.newTarget(new Point(10 - Math.max(50, g.characterControl.breastSize/2)  , 95 - g.characterControl.breastSize/8), g.her.torso.midLayer.rightBreast.nipple, true);
			g.her.rightArmIK.reversedEnd = true;
			g.her.rightArmIK.setEndRotationTarget(-255 + Math.min(120, g.characterControl.breastSize*0.85), [g.her.torso.midLayer.rightBreast.nipple, g.her.torso.midLayer.rightBreast, g.her.torso.midLayer, g.her.torso, g.her]);
			
			}
			
			
		}
		
		public function leftArmBreast(...args):void {
			if(m.main.animtools_comm && m.main.animtools_comm.versionnum > 31)
			{
				g.her.setLeftArmPosition(5, true);   //5 is cupping
			}
			else
			{
				g.her.setLeftArmPosition(1, true);
				g.her.leftArmIK.newTarget(new Point(10 - Math.max(50, g.characterControl.breastSize/2)  , 95 - g.characterControl.breastSize/8), g.her.torsoBack.leftBreast.nipple, true);
				g.her.leftArmIK.reversedEnd = true;
				g.her.leftArmIK.setEndRotationTarget(-255 + Math.min(120, g.characterControl.breastSize*0.85), [g.her.torsoBack.leftBreast.nipple, g.her.torsoBack.leftBreast, g.her.torsoBack,  g.her]);			
			}
		}
		
		
		public function armsBreast(...args):void {
			leftArmBreast();
			rightArmBreast();
		}
		
		public function armsHisCrotch(...args):void {
			g.her.setRightArmPosition(2);
			g.her.setLeftArmPosition(2);
			g.inGameMenu.updateArmsList();
			g.her.rightArmIK.newTarget(new Point(g.her.rightHandOnHim.x - 20, g.her.rightHandOnHim.y - 100), g.him.leftLeg, true);
			g.her.leftArmIK.newTarget(new Point(g.her.leftHandOnHim.x - 20, g.her.leftHandOnHim.y - 100), g.him.rightLeg, true);
			g.her.rightArmIK.setEndRotationTarget(-170, [g.him.leftLeg]);
			g.her.leftArmIK.setEndRotationTarget(-185, [g.him.rightLeg]);
			g.her.rightArmIK.reversedEnd = false;
			g.her.leftArmIK.reversedEnd = false;
		}
		
		public function armsCrotch(...args):void {
			g.her.setArmPosition(1);
			g.her.setArmPosition(-1);
			g.her.leftArmIK.newTarget(new Point(-100, 300), g.her.torso.back, true);
			g.her.leftArmIK.setEndRotationTarget(50, [g.her.torso.back, g.her.torso]);
		}
		
		public function rubOn(...args):void {
			rubbing = true;
			g.her.setRightArmPosition(2);
			g.her.rightArmIK.reversedEnd = false;
			g.her.rightArmIK.setEndRotationTarget(-170, [g.him.leftLeg]);
			g.inGameMenu.updateArmsList();
			m.mc.addEventListener(Event.ENTER_FRAME, rubCrotch);
		}
		
		public function rubOff(...args):void {
			if (rubbing) {
				rubbing = false;
				m.mc.removeEventListener(Event.ENTER_FRAME, rubCrotch);
				g.her.setRightArmPosition(2);
			}
		}
		
		public function rubCrotch(e:Event = null):void {
			var cos:Number = Math.cos(delta);
			g.her.rightArmIK.newTarget(new Point(g.her.rightHandOnHim.x - 10 + cos * 10, g.her.rightHandOnHim.y - 50 + cos * 50), g.him.leftLeg, true);
			g.her.rightArmIK.setEndRotationTarget(-170, [g.him.leftLeg]);
			delta = delta + 0.05;
		}
	}

}