package  
{
	import flash.net.URLRequest;
	import flash.display.Loader;
	import flash.events.IOErrorEvent;
	import flash.events.Event;
	import flash.display.BitmapData;
	/**
	 * ...
	 * @author Pimgd
	 */
	public class BackgroundActions extends CustomTriggerHolder
	{
		private var backgroundToLoad:String;
		private var bgloader:Loader;
		private var urlr:URLRequest;
		private var hasLineListener:Boolean = false;
		public function BackgroundActions(G:Object, M:Main) 
		{
			super(G, M);
			bgloader = new Loader();
			bgloader.contentLoaderInfo.addEventListener(Event.COMPLETE, this.onOBG);
			bgloader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, this.bgtestFail);
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("CHANGE_BACKGROUND", 0, new FunctionObject(changeBackground, this, []));
			//FADE_BACKGROUND_<hex> defined in FadeTriggers.as
		}
		
		public function registerVariables(v:VariableManager):void {
			v.registerVariableWrite("da.background.load", new FunctionObject(setBackground, this, []));
		}
		
		public function changeBackground(...args):void {
			if (backgroundToLoad != "") {
				loadBackground(backgroundToLoad);
			} else if (!hasLineListener) {
				m.displayMessageRed("No background set for loading. (Error: [CHANGE/FADE_BACKGROUND] without da.background.load set)");
			}
		}
		
		public function setBackground(bg:String):void {
			backgroundToLoad = bg;
			m.addLineChangeListener(new FunctionObject(onLineChange, this, []));
			hasLineListener = true;
			
		}
		
		public function onLineChange(...args):void {
			if (g.dialogueControl.sayingPhrase.indexOf("[CHANGE_BACKGROUND]") == -1 && g.dialogueControl.sayingPhrase.indexOf("[FADE_BACKGROUND_") == -1) {
				loadBackground(backgroundToLoad);
			}
			hasLineListener = false;
		}
		
		private function loadBackground(background:String):void {
			urlr = new URLRequest(m.getFileReferenceHandler().convertFilePath(background));
			bgloader.load(urlr);
		}
		
		public function bgtestFail(e:Event = null):void {
			m.displayMessageRed("Couldn't find BG "+urlr.url);
		}
		public function onOBG(param1:Event = null):void {
			g.customElementLoader.customBGHeight = param1.target.content.height;
            var _loc_2:* = new BitmapData(700, param1.target.content.height, false, 4278190080);
            _loc_2.draw(param1.target.content);
			g.customElementLoader.bgImportSource = _loc_2;
            g.customElementLoader.generateCustomBackground();
            g.customElementLoader.bgLoaderComplete(param1);
		}
		
	}

}