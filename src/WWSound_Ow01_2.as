package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_Ow01_2.swf",symbol="WWSound_Ow01_2")]
	
	public dynamic class WWSound_Ow01_2 extends Sound {
		
		public function WWSound_Ow01_2() {
			super();
		}
	}

}
