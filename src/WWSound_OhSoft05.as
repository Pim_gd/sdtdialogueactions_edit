package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhSoft05.swf",symbol="WWSound_OhSoft05")]
	
	public dynamic class WWSound_OhSoft05 extends Sound {
		
		public function WWSound_OhSoft05() {
			super();
		}
	}

}
