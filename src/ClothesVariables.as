package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class ClothesVariables extends CustomVariableHolder
	{
		public static var clothDict:Array = new Array();
		public function ClothesVariables(G:Object, M:Main) 
		{
			super(G, M);
		}
		
		public function registerVariables(v:VariableManager):void {
			var clothTypes:Array = ["ankleCuffs", "armwear", "bellyPiercing", "bottoms", "collar", "cuffs", "earring", "eyewear", "footwear", "gag", "headwear", "himBottoms", "himFootwear", "himTop", "legwear", "legwearB", "nipplePiercing", "panties", "tonguePiercing", "top", "tops"];
			var functionsPerColorComponent:Array = [getClothesColorComponent, getClothesSecondColorComponent, setClothesColorComponent, setClothesSecondColorComponent];
			var type:Array = ["get", "get", "set", "set"];
			var colorComponents:Array = ["r", "g", "b", "a"];
			var getfunctionsPerClothType:Array = [getClothesName];
			var varNameForGetFunctionPerClothType:Array = ["type"];
			var setfunctionsPerClothType:Array = [setClothesName];
			var varNameForSetFunctionPerClothType:Array = ["type"];
			
			for (var i:uint = 0, isize:uint = clothTypes.length; i < isize; i++) {
				for (var j:uint = 0, jsize:uint = functionsPerColorComponent.length; j < jsize; j++) {
					for (var k:uint = 0, ksize:uint = colorComponents.length; k < ksize; k++) {
						var fObject:FunctionObject = new FunctionObject(functionsPerColorComponent[j], this, [clothTypes[i], colorComponents[k]]);
						var varName:String = "da.clothes." + clothTypes[i] + "." + colorComponents[k];
						if (j % 2 == 1) {
							varName += "2";//yeah, that's fucked up
						}
						if (type[j] == "get") {
							v.registerVariableRead(varName, fObject);
						}
						if (type[j] == "set") {
							v.registerVariableWrite(varName, fObject);
						}
					}
				}
				for (var l:uint = 0, lsize:uint = getfunctionsPerClothType.length; l < lsize; l++) {
					var fObject:FunctionObject = new FunctionObject(getfunctionsPerClothType[l], this, [clothTypes[i]]);
					var varName:String = "da.clothes." + clothTypes[i] + "." + varNameForGetFunctionPerClothType[l];
					v.registerVariableRead(varName, fObject);
				}
				for (var m:uint = 0, msize:uint = setfunctionsPerClothType.length; m < msize; m++) {
					var fObject:FunctionObject = new FunctionObject(setfunctionsPerClothType[m], this, [clothTypes[i]]);
					var varName:String = "da.clothes." + clothTypes[i] + "." + varNameForSetFunctionPerClothType[m];
					v.registerVariableWrite(varName, fObject);
				}
				var fObject:FunctionObject = new FunctionObject(getClothDataString, this, [clothTypes[i]]);
				var varName:String = "da.clothes." + clothTypes[i];
				v.registerVariableRead(varName, fObject);
				fObject = new FunctionObject(setClothDataString, this, [clothTypes[i]]);
				varName = "da.clothes." + clothTypes[i];
				v.registerVariableWrite(varName, fObject);
			}
		}
		
		public function getClothDataString(clothType:String):String {
			var clothObject:Object = getClothObject(clothType);
			return clothObject.getDataString();
		}
		
		public function setClothDataString(clothType:String, value:*):String {
			var clothObject:Object = getClothObject(clothType);
			var clothString:String = checkClothString(clothType, value);
			clothObject.loadDataString(clothString);
			return clothObject.getDataString();
		}
		
		public function getClothesName(clothType:String):String {
			var clothObject:Object = getClothObject(clothType);
			return clothObject.selectedName;
		}
		
		public function setClothesName(clothType:String, value:*):int {
			var clothObject:Object = getClothObject(clothType);
			var index:int = clothObject.elementNameList.indexOf(value);
			if (index != -1) {
				clothObject.selection = index;
				if (clothObject.associatedModType)
				{
					g.customElementLoader.clearModTypes([clothObject.associatedModType]);
				}
				clothObject.resetElement();
			} else {
				m.displayMessageRed("Can't find clothing piece (" + value + ") for " + clothType);
			}
			
			return clothObject.selection;
		}
		
		public function setClothesColorComponent(clothType:String, component:String, value:*):Number {
			var clothObject:Object = getClothObject(clothType);
			if (value is String && !isNaN(Number(value))) {
				var currentVal:Number = clothObject.rgb1[component];
				var minVal:Number = 0;
				var maxVal:Number = component == "a" ? 1 : 255;//1 for alpha, 255 for the rest.
				var newVal:Number = 0;
				if ((value as String).charAt(0) == "-" || (value as String).charAt(0) == "+" ) {
					newVal = currentVal + Number(value);
				} else {
					newVal = Number(value);
				}
				newVal = Math.min(newVal, maxVal);
				newVal = Math.max(newVal, minVal);
				clothObject.rgb1[component] = newVal;
			} else {
				clothObject.rgb1[component] = value;
			}
			clothObject.resetFills();
			return clothObject.rgb1[component];
		}
		
		public function setClothesSecondColorComponent(clothType:String, component:String, value:*):Number {
			var clothObject:Object = getClothObject(clothType);
			if (value is String && !isNaN(Number(value))) {
				var currentVal:Number = clothObject.rgb2[component];
				var minVal:Number = 0;
				var maxVal:Number = component == "a" ? 1 : 255;//1 for alpha, 255 for the rest.
				var newVal:Number = 0;
				if ((value as String).charAt(0) == "-" || (value as String).charAt(0) == "+" ) {
					newVal = currentVal + Number(value);
				} else {
					newVal = Number(value);
				}
				newVal = Math.min(newVal, maxVal);
				newVal = Math.max(newVal, minVal);
				clothObject.rgb2[component] = newVal;
			} else {
				clothObject.rgb2[component] = value;
			}
			clothObject.resetFills();
			return clothObject.rgb2[component];
		}
		
		public function getClothesColorComponent(clothType:String, component:String):Number {
			var clothObject:Object = getClothObject(clothType);
			return clothObject.rgb1[component];
		}
		
		public function getClothesSecondColorComponent(clothType:String, component:String):Number {
			var clothObject:Object = getClothObject(clothType);
			return clothObject.rgb2[component];
		}
		
		public function getClothObject(clothType:String):Object {
			initializeClothes(g, m);
			return clothDict[clothType];
		}
		
		public function checkClothString(key:String, value:String):String {
			var valsplitlength:uint = value.split(",").length;
			if (valsplitlength == 9) {
				return checkClothStringA(key, value, 8);
			} else if (valsplitlength == 7) {
				return checkClothStringA(key, value, 6);
			} else if (valsplitlength == 5) {
				return checkClothStringA(key, value, 4);
			} else if (valsplitlength == 4) {
				return checkClothStringA(key, value, 3);
			} else {
				m.displayMessageRed("Can't identify format for clothing set - expected either 4, 5, 7 or 9 values, got " + valsplitlength + " values.");
				return "";
			}
		}
		
		private function checkClothStringA(key:String, value:String, required:int):String {
			var newValues:Array = value.split(",");
			var currentValues:Array = clothDict[key].getDataString().split(",");
			if (newValues.length == 1) {
				currentValues[0] = newValues[0];
			} else if (newValues.length >= required + 1) {
				if (required == 3 || required == 4 || required == 8) {
					for (var i:int = 0; i < required + 1; i++) {
						currentValues[i] = newValues[i];
					}
				} else if (required == 6) {
					for (var i:int = 0; i <= 3; i++) {
						currentValues[i] = newValues[i];
					}
					currentValues[4] = 1;
					for (var i:int = 4; i < required + 1; i++) {
						currentValues[i + 1] = newValues[i];
					}
					currentValues[8] = 1;
				}
			}
			//main.updateStatus(currentValues.toString());
			return currentValues.toString();
		}
		
		public static function initializeClothes(G:Object, M:Main):void {
			var g:Object = G;
			var m:Main = M;
			if (clothDict.length == 0) {
				clothDict["panties"] = g.characterControl.pantiesControl;// .getDataString();
				clothDict["bottoms"] = g.characterControl.bottomsControl;// .getOptionalDataString("bottoms") + "";
				clothDict["legwear"] = g.characterControl.legwearControl;// .getDataString();
				clothDict["legwearB"] = g.characterControl.legwearBControl;// .getOptionalDataString("legwearB") + "";
				clothDict["footwear"] = g.characterControl.footwearControl;// .getDataString();
				clothDict["top"] = g.characterControl.braControl;// .getDataString();
				clothDict["tops"] = g.characterControl.topControl;// .getOptionalDataString("tops");
				clothDict["armwear"] = g.characterControl.armwearControl;// .getDataString();
				clothDict["eyewear"] = g.characterControl.eyewearControl;// .getDataString();
				clothDict["headwear"] = g.characterControl.headwearControl;// .getDataString();
				clothDict["tonguePiercing"] = g.characterControl.tonguePiercingControl;// .getDataString();
				clothDict["nipplePiercing"] = g.characterControl.nipplePiercingControl;// .getOptionalDataString("nipplePiercing");
				clothDict["bellyPiercing"] = g.characterControl.bellyPiercingControl;// .getOptionalDataString("bellyPiercing");
				clothDict["earring"] = g.characterControl.earringControl;// .getOptionalDataString("earring");
				clothDict["collar"] = g.characterControl.collarControl;// .getDataString();
				clothDict["cuffs"] = g.characterControl.cuffsControl;// .getDataString();
				clothDict["ankleCuffs"] = g.characterControl.ankleCuffsControl;// .getOptionalDataString("ankleCuffs");
				clothDict["gag"] = g.characterControl.gagControl;// .getDataString();
				clothDict["himFootwear"] = g.him.currentBody.footwearControl;// .getDataString();
				//TODO the him clothes I forgot but you have to init when they are there
			}
			var himMaleBody:Class = m.getLoaderRef().eDOM.getDefinition("obj.him.bodies.HimMaleBody") as Class;
			if (g.him.currentBody != null && g.him.currentBody is himMaleBody) {
				if (g.him.currentBody.topControl != null) {
					clothDict["himTop"] = g.him.currentBody.topControl;
				}
				if (g.him.currentBody.bottomsControl != null) {
					clothDict["himBottoms"] = g.him.currentBody.bottomsControl;
				}
			}
		}
		
	}

}