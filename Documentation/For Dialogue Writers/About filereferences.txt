One of the features that DialogueActions contains is the ability to load external files from a Dialogue.
This feature is used for loading hairs, dialogues and backgrounds on the fly.

How do you use these?
Well, lets say you want to have DA load a background.
This is done by setting the background variable, like so:
{"set":{"da.background.load":"filename.png"}}

The background variable should be set to value containing a name or path of a background image.
The FULL path is
(Loader.swf location)/Mods/(Last loaded CharacterFolder)/<backgroundvariable>
Just install DialogueActions in your $INIT$ folder.
So lets say you have your DialogueActions dialogue in a testing folder, called "LoaderDialogueMods".
And the loader is installed in a folder named SDT. What's above that isn't really interesting.
The path we'd be looking for would be 
SDT/Mods/LoaderDialogueMods/<backgroundvariable>

If you set background to "bedroom.png", the path becomes
SDT/Mods/LoaderDialogueMods/bedroom.png

This means that you should add a bedroom.png image file to the folder you store the dialogue that uses DialogueActions.

If you want to use multiple character folders, you can prefix the path with $. Like so:
{"set":{"da.background.load":"$CharFolder/filename.png"}}

This would load a background "filename.png" from the character folder "CharFolder". Specifically, DialogueActions expands the path to
(Loader.swf location)/Mods/<backgroundvariable> - after stripping the $.

This means that if you want to target something in the $INIT$ folder, you'd do it like so:
{"set":{"da.background.load":"$$INIT$/filename.png"}}

When bundling files with your DialogueActions, create a character folder and store everything in there.
Do NOT place files in the $INIT$ folder. If everyone did that, people's $INIT$ folders would get clogged up, and your dialogue may get loaded or may load something you didn't intend.